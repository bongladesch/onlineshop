package com.bongladesch.onlineshop.sign;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("rest")
public class SignServiceApplication extends Application
{
	@Override
	public Set<Class<?>> getClasses()
	{
        Set<Class<?>> s = new HashSet<Class<?>>();
        s.add(SignService.class);
        return s;
	}
}