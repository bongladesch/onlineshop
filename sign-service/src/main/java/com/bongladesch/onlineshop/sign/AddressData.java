package com.bongladesch.onlineshop.sign;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AddressData
{
	@XmlElement public long id;
	@XmlElement public String street;
	@XmlElement public String firstName;
	@XmlElement public String lastName;
	@XmlElement public String userName;
	@XmlElement public String number;
	@XmlElement public String zipcode;
	@XmlElement public String country;
	@XmlElement public String city;
}
