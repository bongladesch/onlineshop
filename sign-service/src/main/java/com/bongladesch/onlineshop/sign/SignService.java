package com.bongladesch.onlineshop.sign;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("/sign")
public class SignService
{
	private static Logger logger = Logger.getLogger("signServiceLogger");
	private static final String userTarget = "http://" + getUserRepositoryURL() + ":8080/user-repository";

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response register(UserData data)
	{
		logger.log(Level.INFO, "Create new customer called: " + data.userName);
		Client client = ClientBuilder.newClient();
		String cartTarget = "http://" + getCartRepositoryURL() + ":8080/cart-repository";
		WebTarget webUserTarget = client.target(userTarget).path("/rest/users");
		WebTarget webCartTarget = client.target(cartTarget).path("/rest/carts").queryParam("username", data.userName);
		Invocation.Builder userInvocationBuilder = webUserTarget.request(MediaType.APPLICATION_JSON);
		Invocation.Builder cartInvocationBuilder = webCartTarget.request(MediaType.APPLICATION_JSON);
		cartInvocationBuilder.post(Entity.entity(null, MediaType.APPLICATION_JSON));
		data.admin = false;
		return userInvocationBuilder.post(Entity.entity(data, MediaType.APPLICATION_JSON));
	}

	@POST
	@Path("/login")
	@Produces("application/json")
	public Response login(@QueryParam("username")String userName, @QueryParam("password")String password)
	{
		logger.log(Level.INFO, "Login of user " + userName);
		Client client = ClientBuilder.newClient();
		WebTarget webUserTarget = client.target(userTarget).path("/rest/users/" + userName);
		UserData user = webUserTarget.request(MediaType.APPLICATION_JSON).get().readEntity(UserData.class);
		if(user.password.equals(password))
		{
			String message = "Login success";
			if(user.admin) return Response.status(Response.Status.OK).entity(getMessageObject(message)).build();
			else return Response.status(Response.Status.ACCEPTED).entity(getMessageObject(message)).build();
			
		}
		else
		{
			String message = "Login failed";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	private static String getUserRepositoryURL()
	{
		String url = System.getenv("USER_REPOSITORY_URL");
		if(url != null) return url;
		else return "user-repository";
	}

	private static String getCartRepositoryURL()
	{
		String url = System.getenv("CART_REPOSITORY_URL");
		if(url != null) return url;
		else return "cart-repository";
	}

	private static String getMessageObject(String message)
	{
		return "{\"message\":\"" + message + "\"}";
	}
}