package com.bongladesch.onlineshop.sign;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserData
{
	@XmlElement public String userName;
	@XmlElement public String email;
	@XmlElement public String firstName;
	@XmlElement public String lastName;
	@XmlElement public String password;
	@XmlElement public boolean admin;
	@XmlElement public List<AddressData> addresses;
}