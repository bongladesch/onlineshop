package com.bongladesch.onlineshop.shopping;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("rest")
public class ShoppingServiceApplication extends Application
{
	@Override
	public Set<Class<?>> getClasses()
	{
        Set<Class<?>> s = new HashSet<Class<?>>();
        s.add(ShoppingService.class);
        return s;
	}
}