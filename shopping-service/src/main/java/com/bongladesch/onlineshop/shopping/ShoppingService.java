package com.bongladesch.onlineshop.shopping;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("/shopping")
public class ShoppingService
{
	private static Logger logger = Logger.getLogger("shoppingServiceLogger");
	private static final String cartTarget = "http://" + getCartRepositoryURL() + ":8080/cart-repository";
	private static final String orderTarget = "http://" + getOrderRepositoryURL() + ":8080/order-repository";
	private static final String articleTarget = "http://" + getArticleRepositoryURL() + ":8080/article-repository";

	@POST
	@Path("/order")
	@Produces("application/json")
	public Response orderCart(@QueryParam("username")String userName, @QueryParam("address")long id)
	{
		long orderId = generateId();
		CartData cart = getCart(userName);
		OrderData order = new OrderData();
		order.id = orderId;
		order.userName = userName;
		order.addressId = id;
		order.status = "IN_WORK";
		order.orderEntries = new ArrayList<OrderEntryData>();
		List<ArticleData> updatedArticles = new ArrayList<ArticleData>();
		for(CartEntryData entry : cart.cartEntries)
		{
			ArticleData article = getArticle(entry.articleId);
			if(article.stock >= entry.number)
			{
				article.stock -= entry.number;
				article.purchases += entry.number;
				updatedArticles.add(article);
			}
			else
			{
				String message = "Artile " + article.id + " not available in amount of " + entry.number;
				return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
			}
			OrderEntryData newEntry = new OrderEntryData();
			newEntry.orderId = orderId;
			newEntry.articleId = entry.articleId;
			newEntry.price = entry.price;
			newEntry.number = entry.number;
			order.orderEntries.add(newEntry);
		}
		for(ArticleData updatedArticle : updatedArticles)
		{
			updateArticle(updatedArticle);
		}
		clearCart(userName);
		Client client = ClientBuilder.newClient();
		WebTarget webOrderTarget = client.target(orderTarget).path("/rest/orders");
		Invocation.Builder orderInvocationBuilder = webOrderTarget.request(MediaType.APPLICATION_JSON);
		Response response = orderInvocationBuilder.post(Entity.entity(order, MediaType.APPLICATION_JSON));
		if(response.getStatus() == Response.Status.OK.getStatusCode())
		{
			String message = "Order cart of user " + userName + " successfully";
			return Response.status(Response.Status.OK).entity(getMessageObject(message)).build();
		}
		else
		{
			String message = "Order cart of user " + userName + " failed";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	@GET
	@Produces("application/json")
	public Response viewCart(@QueryParam("username")String userName)
	{
		CartData cart = getCart(userName);
		return Response.status(Response.Status.OK).entity(cart).build();
	}

	@PUT
	@Produces("application/json")
	public Response addArticleToCart(@QueryParam("username")String userName, @QueryParam("article")String articleId, @QueryParam("number")int number)
	{
		CartData cart = getCart(userName);
		CartEntryData entry = new CartEntryData();
		entry.articleId = articleId;
		entry.price = getArticle(articleId).price;
		entry.userName = userName;
		entry.number = number;
		if(cart.cartEntries == null) cart.cartEntries = new ArrayList<CartEntryData>();
		cart.cartEntries.add(entry);
		Response response = updateCart(cart);
		if(response.getStatus() == Response.Status.OK.getStatusCode())
		{
			String message = "Article " + articleId + " added to cart of user: " + userName;
			return Response.status(Response.Status.OK).entity(getMessageObject(message)).build();
		}
		else
		{
			String message = "Add article " + articleId + " to cart of user " + userName + " failed";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	@DELETE
	@Produces("application/json")
	public Response removeArticleFromCart(@QueryParam("username")String userName, @QueryParam("article")String articleId)
	{
		CartData cart = getCart(userName);
		boolean found = false;
		for(int index = 0; index < cart.cartEntries.size(); index++)
		{
			CartEntryData entry = cart.cartEntries.get(index);
			if(entry.articleId.equals(articleId))
			{
				cart.cartEntries.remove(index);
				found = true;
				break;
			}
		}
		Response response = updateCart(cart);
		if(response.getStatus() == Response.Status.OK.getStatusCode() && found)
		{
			String message = "Article " + articleId + " removed from cart of user: " + userName;
			return Response.status(Response.Status.OK).entity(getMessageObject(message)).build();
		}
		else
		{
			String message = "Remove article " + articleId + " from cart of user " + userName + " failed";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	@DELETE
	@Path("/clear")
	@Produces("application/json")
	public Response clearCart(@QueryParam("username")String userName)
	{
		CartData cart = getCart(userName);
		cart.cartEntries.clear();
		Response response  = updateCart(cart);
		if(response.getStatus() == Response.Status.OK.getStatusCode())
		{
			String message = "Cart cleared of user " + userName;
			return Response.status(Response.Status.OK).entity(getMessageObject(message)).build();
		}
		else
		{
			String message = "Clear cart of user " + userName + " failed";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	private static long generateId()
	{
		Random random = new Random();
		long id = random.nextLong();
		if(!idIsAlreadyUsed(id)) return id;
		else return generateId();
	}

	private static boolean idIsAlreadyUsed(long id)
	{
		Client client = ClientBuilder.newClient();
		WebTarget webOrderTarget = client.target(orderTarget).path("/rest/orders/" + id);
		Response response = webOrderTarget.request(MediaType.APPLICATION_JSON).get();
		if(response.getStatus() == Response.Status.OK.getStatusCode()) return true;
		else return false;
	}

	private static CartData getCart(String userName)
	{
		logger.log(Level.INFO, "Get cart by username " + userName);
		Client client = ClientBuilder.newClient();
		WebTarget webUserTarget = client.target(cartTarget).path("/rest/carts/" + userName);
		Response response = webUserTarget.request(MediaType.APPLICATION_JSON).get();
		logger.info(response.toString());
		return response.readEntity(CartData.class);
	}

	private static Response updateCart(CartData cart)
	{
		logger.log(Level.INFO, "Update cart with username " + cart.userName);
		Client client = ClientBuilder.newClient();
		WebTarget webCartTarget = client.target(cartTarget).path("/rest/carts");
		Invocation.Builder cartInvocationBuilder = webCartTarget.request(MediaType.APPLICATION_JSON);
		return cartInvocationBuilder.put(Entity.entity(cart, MediaType.APPLICATION_JSON));
	}

	private static ArticleData getArticle(String id)
	{
		logger.log(Level.INFO, "Get Article with id " + id);
		Client client = ClientBuilder.newClient();
		WebTarget webArticleTarget = client.target(articleTarget).path("/rest/articles/" + id);
		Invocation.Builder articleInvocationBuilder = webArticleTarget.request(MediaType.APPLICATION_JSON);
		return articleInvocationBuilder.get().readEntity(ArticleData.class);
	}

	private static Response updateArticle(ArticleData article)
	{
		logger.log(Level.INFO, "Update Article with id " + article.id);
		Client client = ClientBuilder.newClient();
		WebTarget webArticleTarget = client.target(articleTarget).path("/rest/articles");
		Invocation.Builder articleInvocationBuilder = webArticleTarget.request(MediaType.APPLICATION_JSON);
		Response response = articleInvocationBuilder.put(Entity.entity(article, MediaType.APPLICATION_JSON));
		if(response.getStatus() == Response.Status.OK.getStatusCode())
		{
			String message = "Article with id " + article.id + " updated";
			return Response.status(Response.Status.OK).entity(getMessageObject(message)).build();
		}
		else
		{
			String message = "Update article with id " + article.id + " failed";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	private static String getCartRepositoryURL()
	{
		String url = System.getenv("CART_REPOSITORY_URL");
		if(url != null) return url;
		else return "cart-repository";
	}

	private static String getOrderRepositoryURL()
	{
		String url = System.getenv("ORDER_REPOSITORY_URL");
		if(url != null) return url;
		else return "order-repository";
	}

	private static String getArticleRepositoryURL()
	{
		String url = System.getenv("ARTICLE_REPOSITORY_URL");
		if(url != null) return url;
		else return "article-repository";
	}

	private static String getMessageObject(String message)
	{
		return "{\"message\":\"" + message + "\"}";
	}
}