package com.bongladesch.onlineshop.shopping;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ArticleData
{
	@XmlElement public String id;
	@XmlElement public String name;
	@XmlElement public String description;
	@XmlElement public double price;
	@XmlElement public long stock;
	@XmlElement public long purchases;
	@XmlElement public List<String> pictures;
	@XmlElement public List<String> categories;
}