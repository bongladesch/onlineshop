package com.bongladesch.onlineshop.article;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("/article")
public class ArticleService
{
	private static Logger logger = Logger.getLogger("articleServiceLogger");
	private static final String articleTarget = "http://" + getArticleRepositoryURL() + ":8080/article-repository";

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response addArticle(ArticleData article)
	{
		logger.log(Level.INFO, "Add article with id " + article.id);
		Client client = ClientBuilder.newClient();
		WebTarget webArticleTarget = client.target(articleTarget).path("/rest/articles");
		Invocation.Builder articleInvocation = webArticleTarget.request(MediaType.APPLICATION_JSON);
		Response response = articleInvocation.post(Entity.entity(article, MediaType.APPLICATION_JSON));
		if(response.getStatus() == Response.Status.OK.getStatusCode())
		{
			String message = "Article with id " + article.id + " added";
			return Response.status(Response.Status.OK).entity(getMessageObject(message)).build();
		}
		else
		{
			String message = "Add article with id " + article.id + " failed";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	@PUT
	@Consumes("application/json")
	@Produces("application/json")
	public Response updateArticle(ArticleData article)
	{
		logger.log(Level.INFO, "Update article with id " + article.id);
		Client client = ClientBuilder.newClient();
		WebTarget webArticleTarget = client.target(articleTarget).path("/rest/articles");
		Invocation.Builder articleInvocation = webArticleTarget.request(MediaType.APPLICATION_JSON);
		Response response = articleInvocation.put(Entity.entity(article, MediaType.APPLICATION_JSON));
		if(response.getStatus() == Response.Status.OK.getStatusCode())
		{
			String message = "Article with id " + article.id + " updated";
			return Response.status(Response.Status.OK).entity(getMessageObject(message)).build();
		}
		else
		{
			String message = "Update article with id " + article.id + " failed";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	@PUT
	@Path("/stock")
	@Produces("application/json")
	public Response updateStock(@QueryParam("id")String id, @QueryParam("stock")int stock)
	{
		Client client = ClientBuilder.newClient();
		WebTarget webOrderTarget = client.target(articleTarget).path("/rest/articles/" + id);
		Invocation.Builder orderInvocationBuilder = webOrderTarget.request(MediaType.APPLICATION_JSON);
		ArticleData article = orderInvocationBuilder.get().readEntity(ArticleData.class);
		article.stock = stock;
		return updateArticle(article);
	}

	@DELETE
	@Produces("application/json")
	public Response deleteArticle(@QueryParam("id")String id)
	{
		logger.log(Level.INFO, "Delete article with id " + id);
		Client client = ClientBuilder.newClient();
		WebTarget webArticleTarget = client.target(articleTarget).path("/rest/articles").queryParam("id", id);
		Invocation.Builder articleInvocation = webArticleTarget.request(MediaType.APPLICATION_JSON);
		Response response = articleInvocation.delete();
		if(response.getStatus() == Response.Status.OK.getStatusCode())
		{
			String message = "Article with id " + id + " deleted";
			return Response.status(Response.Status.OK).entity(getMessageObject(message)).build();
		}
		else
		{
			String message = "Delete article with id " + id + " failed";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	private static String getArticleRepositoryURL()
	{
		String url = System.getenv("ARTICLE_REPOSITORY_URL");
		if(url != null) return url;
		else return "article-repository";
	}

	private static String getMessageObject(String message)
	{
		return "{\"message\":\"" + message + "\"}";
	}
}