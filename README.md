# Onlineshop

Die ist das Backend des Online-Shop-Systems in Microservice Architektur aus der Projektarbeit "Skallierbare Webanwendungen mit Java-EE und Docker" von Ken Brucksch.

## Voraussetzungen
Um die einzelnen Services starten zu können ist lediglich eine installierte Docker-Engine und das Orchestrierungstool Docker-Compose notwending.
Sämtliche Informationen zur Installation finden Sie [hier](https://docs.docker.com/engine/installation/).

## Getting Started
Jeder Microservice ist ein einzelnes Gradle-Projekt. In jedem Projektordner befindet sich ein Skript im "run"-Verzeichnis mit dem der entsprechende Service gestartet werden kann. Dieses Skript muss aus dem Root-Verzeichnis des Unterprojekts gestartet werden. Beispiel:
```
cd user-repository
./run/run.sh
```
## Request-Handler
Die Kommunikation mit einem Frontend soll über den Request-Handler geschehen. Dieser ist allerdings nur für die Benutzung durch ein Frontend verwendbar, da die Authentifizierung über Cookies stattfindet. Damit das System trotzdem getestet werden kann sind sämtliche Ports der einzelnen Services wie folgt gemappt, der Request-Handler ist daduch beim testen nicht nötig.

### Port-Mapping
```
BACKEND:
    UserRepository: 8080
    CartRepository: 8081
    ArticleRepository: 8082
    OrderRepository: 8083

MIDDLEWARE:
    RequestHandler: 8090
    SignService: 8092
    CustomerService: 8093
    AdminService: 8094
    ShoppingService: 8095
    SearchService: 8096
    ArticleService: 8097
    OrderService: 8098
```

## Beispiel-Request
Anlegen eines Kunden:
```
POST http://localhost:8092/sign-service/rest/sign
{
  "userName":"myuser",
  "firstName":"Ken",
  "lastName":"Brucksch",
  "email":"ken.brucksch@mail.com",
  "password":"mypassword",
  "addresses":[]
}
```
Dafür sind die Services: User-Repository, Cart-Repository und Sign-Service nötig, wie es sich aus der Service-View der Dokumentation ergibt.
