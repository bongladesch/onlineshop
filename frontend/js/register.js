/**
 * Created by ken on 06.06.17.
 */
$(document).ready(function ()
{
    var userName = $("#username").val();
    var email = $("#email").val();
    var firstname = $("#firstname").val();
    var lastname = $("#lastname").val();
    var password = $("#password").val();
    var confirm = $("#confirm").val();
    $($("#registerbutton")).click(function ()
    {
        $.ajax({
            type: 'POST',
            url: "localhost:8080/user-repository/resources/users/register",
            dataType: JSON,
            data: {
                "username": userName,
                "email": email,
                "firstname": firstname,
                "lastname": lastname,
                "password": password
            },
            success: function (data) {
                alert("success");
                console.log(data)
            },
            error: function (e) {
                alert("error" + e)
                console.log(e)
            }
        });
    });
});