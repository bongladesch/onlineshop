package com.bongladesch.onlineshop.customer;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Stateless
@Path("/customer")
public class CustomerService
{
	private static Logger logger = Logger.getLogger("customerServiceLogger");
	private static final String userTarget = "http://" + getUserRepositoryURL() + ":8080/user-repository";

	@DELETE
	@Produces("application/json")
	public Response delete(@QueryParam("username")String userName)
	{
		logger.log(Level.INFO, "Delete customer with username: " + userName);
		Client client = ClientBuilder.newClient();
		String cartTarget = "http://" + getCartRepositoryURL() + ":8080/cart-repository";
		WebTarget webUserTarget = client.target(userTarget).path("/rest/users").queryParam("username", userName);
		WebTarget webCartTarget = client.target(cartTarget).path("/rest/carts").queryParam("username", userName);
		Invocation.Builder userInvocationBuilder = webUserTarget.request(MediaType.APPLICATION_JSON);
		Invocation.Builder cartInvocationBuilder = webCartTarget.request(MediaType.APPLICATION_JSON);
		Response response1 = cartInvocationBuilder.delete();
		Response response2 = userInvocationBuilder.delete();
		if(response1.getStatus() == Response.Status.OK.getStatusCode() && response2.getStatus() == Response.Status.OK.getStatusCode())
		{
			String message = "Deletetion of user " + userName + " successfull";
			return Response.status(Response.Status.OK).entity(getMessageObject(message)).build();
		}
		else
		{
			String message = "Deletion of user " + userName + " failed";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	@PUT
	@Consumes("application/json")
	@Produces("application/json")
	public Response update(UserData data)
	{
		logger.log(Level.INFO, "Update customer with username: " + data.userName);
		data.admin = false;
		Client client = ClientBuilder.newClient();
		WebTarget webUserTarget = client.target(userTarget).path("/rest/users");
		Invocation.Builder userInvocationBuilder = webUserTarget.request(MediaType.APPLICATION_JSON);
		Response response = userInvocationBuilder.put(Entity.entity(data, MediaType.APPLICATION_JSON));
		if(response.getStatus() == Response.Status.OK.getStatusCode())
		{
			String message = "Customer with username " + data.userName + " updated";
			return Response.status(Response.Status.OK).entity(getMessageObject(message)).build();
		}
		else
		{
			String message = "Updating customer with username " + data.userName + "failed";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	@Path("/password")
	@PUT
	@Produces("application/json")
	public Response changePassword(@QueryParam("username")String userName, @QueryParam("password")String password)
	{
		logger.log(Level.INFO, "Change password for customer with username: " + userName);
		UserData user = getUser(userName);
		user.password = password;
		Response response = update(user);
		if(response.getStatus() == Response.Status.OK.getStatusCode())
		{
			String message = "Changed password for customer with username: " + userName;
			return Response.status(response.getStatus()).entity(getMessageObject(message)).build();
		}
		else
		{
			String message = "Change password failed for customer with username: " + userName;
			return Response.status(response.getStatus()).entity(getMessageObject(message)).build();
		}
	}

	@POST
	@Path("/address")
	@Consumes("application/json")
	@Produces("application/json")
	public Response addAddress(@QueryParam("username")String userName, AddressData data)
	{
		logger.log(Level.INFO, "Add address to customer " + userName);
		UserData user = getUser(userName);
		user.addresses.add(data);
		Response response = update(user);
		if(response.getStatus() == Response.Status.OK.getStatusCode())
		{
			String message = "Address added for customer with username: " + userName;
			return Response.status(response.getStatus()).entity(getMessageObject(message)).build();
		}
		else
		{
			String message = "Add Address failed for customer with username: " + userName;
			return Response.status(response.getStatus()).entity(getMessageObject(message)).build();
		}
	}

	@DELETE
	@Path("/address")
	@Produces("application/json")
	public Response removeAddress(@QueryParam("username")String userName, @QueryParam("id")long id)
	{
		logger.log(Level.INFO, "Remove address from customer " + userName);
		UserData user = getUser(userName);
		for(int index = 0; index < user.addresses.size(); index++)
		{
			AddressData address = user.addresses.get(index);
			if(address.id == id) user.addresses.remove(index);
		}
		Response response = update(user);
		if(response.getStatus() == Response.Status.OK.getStatusCode())
		{
			String message = "Address removed from customer with username: " + userName;
			return Response.status(response.getStatus()).entity(getMessageObject(message)).build();
		}
		else
		{
			String message = "Remove Address failed for customer with username: " + userName;
			return Response.status(response.getStatus()).entity(getMessageObject(message)).build();
		}
	}

	@GET
	@Produces("application/json")
	public Response viewProfile(@QueryParam("username")String userName)
	{
		UserData user = getUser(userName);
		user.password = null;
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			String json = mapper.writeValueAsString(user);
			return  Response.ok(json, MediaType.APPLICATION_JSON).build();
		}
		catch(JsonProcessingException e)
		{
			logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for customer: " + userName;
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	private static UserData getUser(String userName)
	{
		logger.log(Level.INFO, "Get customer with username: " + userName);
		Client client = ClientBuilder.newClient();
		WebTarget webUserTarget = client.target(userTarget).path("/rest/users/" + userName);
		Invocation.Builder userInvocationBuilder = webUserTarget.request(MediaType.APPLICATION_JSON);
		Response response = userInvocationBuilder.get();
		return response.readEntity(UserData.class);
	}

	private static String getUserRepositoryURL()
	{
		String url = System.getenv("USER_REPOSITORY_URL");
		if(url != null) return url;
		else return "user-repository";
	}

	private static String getCartRepositoryURL()
	{
		String url = System.getenv("CART_REPOSITORY_URL");
		if(url != null) return url;
		else return "cart-repository";
	}

	private static String getMessageObject(String message)
	{
		return "{\"message\":\"" + message + "\"}";
	}
}