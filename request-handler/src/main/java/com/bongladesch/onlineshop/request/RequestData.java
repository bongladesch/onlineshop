package com.bongladesch.onlineshop.request;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RequestData
{
	@XmlElement String service;
	@XmlElement String method;
	@XmlElement String path;
	@XmlElement List<ParamData> params;
	@XmlElement Object entity;
}