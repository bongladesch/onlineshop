package com.bongladesch.onlineshop.request;

import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("rest")
public class RequestHandlerApplication extends Application
{
	@Override
	public Set<Class<?>> getClasses()
	{
		TimeZone.setDefault(TimeZone.getTimeZone("Europe/Berlin"));
        Set<Class<?>> s = new HashSet<Class<?>>();
        s.add(RequestHandler.class);
        return s;
	}
}