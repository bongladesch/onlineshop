package com.bongladesch.onlineshop.request;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

@Stateless
@Path("/request")
public class RequestHandler
{
	private static Logger logger = Logger.getLogger("requestHandlerLogger");

	private static long SESSION_TIMEOUT = MILLISECONDS.convert(20, MINUTES);
	
	private static final String signTarget = "http://" + getSignServiceURL() + ":8080/sign-service";
	private static final String customerTarget = "http://" + getCustomerServiceURL() + ":8080/customer-service";
	private static final String adminTarget = "http://" + getAdminServiceURL() + ":8080/admin-service";
	private static final String shoppingTarget = "http://" + getShoppingServiceURL() + ":8080/shopping-service";
	private static final String orderTarget = "http://" + getOrderServiceURL() + ":8080/order-service";
	private static final String searchTarget = "http://" + getSearchServiceURL() + ":8080/search-service";
	private static final String articleTarget = "http://" + getArticleServiceURL() + ":8080/article-service";

	@PersistenceUnit(unitName = "request-handler")
	private EntityManagerFactory emf;

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	public Response forwardRequest(@CookieParam("token")Cookie cookie, RequestData request)
	{
		logger.log(Level.INFO, "Forward request");
		if(request.service == null)
		{
			String message = "No service defined";
			Response response = Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
			response.getHeaders().add("Access-Control-Allow-Origin", "*");
			return response;
		}
		if(request.path != null && request.path.equals("login")) return login(request);
		String token = null;
		if(cookie != null) token = cookie.getValue();
		if(!accessGranted(token, request.service))
		{
			String message = "Access denied";
			Response response = Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
			response.getHeaders().add("Access-Control-Allow-Origin", "*");
			return response;
		}
		WebTarget webTarget = getTarget(request.service);
		if(webTarget == null)
		{
			String message = "Service \"" + request.service + "\" not found";
			Response response = Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
			response.getHeaders().add("Access-Control-Allow-Origin", "*");
			return response;
		}
		// Set path
		if(request.path == null)
		{
			String message = "No path found";
			Response response = Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
			response.getHeaders().add("Access-Control-Allow-Origin", "*");
			return response;
		}
		else webTarget = webTarget.path("/rest/" + request.path);
		// Set parameters
		if(request.params != null)
		{
			for(ParamData param : request.params)
			{
				webTarget = webTarget.queryParam(param.key, param.value);
			}
		}
		// execute method
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response;
		if(request.entity != null)
		{
			switch(request.method)
			{
				case "PUT":
					response = invocationBuilder.put(Entity.entity(request.entity, MediaType.APPLICATION_JSON));
					break;
				case "POST":
					response = invocationBuilder.post(Entity.entity(request.entity, MediaType.APPLICATION_JSON));
					break;
				default:
					String message = "Method not reachable in this context";
					response = Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
					break;
			}
		}
		else
		{
			switch(request.method)
			{
				case "GET":
					response = invocationBuilder.get();
					break;
				case "PUT":
					response = invocationBuilder.put(null);
					break;
				case "POST":
					response = invocationBuilder.post(null);
					break;
				case "DELETE":
					response = invocationBuilder.delete();
					break;
				default:
					String message = "Method not reachable in this context";
					response = Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
					break;
			}
		}
		response.getHeaders().add("Access-Control-Allow-Origin", "*");
		return response;
	}

	@DELETE
	@Path("/logout")
	@Produces("application/json")
	public Response logout(@CookieParam("token")Cookie cookie)
	{
		Response response;
		if(cookie != null)
		{
			EntityManager em = emf.createEntityManager();
			Session session = getSession(cookie.getValue());
			em.remove(session);
			em.close();
			String message = "Logout successfull";
			response = Response.status(Response.Status.OK).entity(getMessageObject(message)).build();
		}
		else
		{
			String message = "Logout failed";
			response = Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
		response.getHeaders().add("Access-Control-Allow-Origin", "*");
		return response;
	}

	private Response login(RequestData request)
	{
		WebTarget webTarget = getTarget(request.service).path("/rest/customer/" + request.path);
		String userName = null;
		for(ParamData param : request.params)
		{
			if(param.key.equals("username")) userName = param.value;
			webTarget = webTarget.queryParam(param.key, param.value);
		}
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(null);
		if(response.getStatus() == Response.Status.OK.getStatusCode() || response.getStatus() == Response.Status.ACCEPTED.getStatusCode())
		{
			EntityManager em = emf.createEntityManager();
			String token = generateToken(userName);
			Session session = new Session();
			session.setUserName(userName);
			session.setToken(token);
			if(response.getStatus() == Response.Status.OK.getStatusCode()) session.setAdmin(true);
			else session.setAdmin(false);
			em.persist(session);
			em.close();
			return Response.ok("Login successfull").cookie(new NewCookie("token", token)).build();
		}
		return response;
	}

	private static WebTarget getTarget(String service)
	{
		Client client = ClientBuilder.newClient();
		switch(service)
		{
			case "sign": 
				return client.target(signTarget);
			case "customer":
				return client.target(customerTarget);
			case "admin":
				return client.target(adminTarget);
			case "shopping":
				return client.target(shoppingTarget);
			case "order":
				return client.target(orderTarget);
			case "search":
				return client.target(searchTarget);
			case "article":
				return client.target(articleTarget);
			default: return null;
		}
	}

	private boolean accessGranted(String token, String service)
	{
		switch(service)
		{
			case "sign": 
				if(token != null)return updateSession(token);
				else return true;
			case "customer": return isCustomer(token);
			case "admin": return isAdmin(token);
			case "shopping": return isCustomer(token);
			case "order": return isAdmin(token);
			case "search":
				if(token != null)return updateSession(token);
				else return true;
			case "article": return isAdmin(token);
			default: return false;
		}
	}

	private boolean isCustomer(String token)
	{
		Session session = getSession(token);
		if(session == null) return false;
		Calendar now = new GregorianCalendar();
		long sessionTime = now.getTime().getTime() - session.getTimestamp().getTime().getTime();
		if(SESSION_TIMEOUT < sessionTime) return false;
		EntityManager em = emf.createEntityManager();
		session.setTimestamp(new GregorianCalendar());
		em.merge(session);
		em.close();
		return true;
	}

	private boolean isAdmin(String token)
	{
		Session session = getSession(token);
		if(session == null) return false;
		Calendar now = new GregorianCalendar();
		long sessionTime = now.getTime().getTime() - session.getTimestamp().getTime().getTime();
		if(SESSION_TIMEOUT < sessionTime) return false;
		if(session.isAdmin())
		{
			EntityManager em = emf.createEntityManager();
			session.setTimestamp(new GregorianCalendar());
			em.merge(session);
			em.close();
			return true;
		}
		return false;
	}

	private boolean updateSession(String token)
	{
		Session session = getSession(token);
		if(session != null)
		{
			Calendar now = new GregorianCalendar();
			long sessionTime = now.getTime().getTime() - session.getTimestamp().getTime().getTime();
			if(SESSION_TIMEOUT >= sessionTime)
			{
				EntityManager em = emf.createEntityManager();
				session.setTimestamp(new GregorianCalendar());
				em.merge(session);
				em.close();
			}
		}
		return true;
	}
	
	private Session getSession(String token)
	{
		EntityManager em = emf.createEntityManager();
		Session session = em.find(Session.class, token);
		em.close();
		return session;
	}

	private static String generateToken(String userName)
	{
		return "_" + userName + "_" + System.currentTimeMillis();
	}

	private static String getMessageObject(String message)
	{
		return "{\"message\":\"" + message + "\"}";
	}

	/* URLs */

	private static String getSignServiceURL()
	{
		String url = System.getenv("SIGN_SERVICE_URL");
		if(url != null) return url;
		else return "sign-service";
	}

	private static String getCustomerServiceURL()
	{
		String url = System.getenv("CUSTOMER_SERVICE_URL");
		if(url != null) return url;
		else return "customer-service";
	}

	private static String getAdminServiceURL()
	{
		String url = System.getenv("ADMIN_SERVICE_URL");
		if(url != null) return url;
		else return "admin-service";
	}

	private static String getShoppingServiceURL()
	{
		String url = System.getenv("SHOPPING_SERVICE_URL");
		if(url != null) return url;
		else return "shopping-service";
	}

	private static String getOrderServiceURL()
	{
		String url = System.getenv("ORDER_SERVICE_URL");
		if(url != null) return url;
		else return "order-service";
	}

	private static String getSearchServiceURL()
	{
		String url = System.getenv("SEARCH_SERVICE_URL");
		if(url != null) return url;
		else return "search-service";
	}

	private static String getArticleServiceURL()
	{
		String url = System.getenv("ARTICLE_SERVICE_URL");
		if(url != null) return url;
		else return "article-service";
	}
}