package com.bongladesch.onlineshop.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ParamData
{
	@XmlElement String key;
	@XmlElement String value;
}