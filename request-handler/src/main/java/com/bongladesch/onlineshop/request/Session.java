package com.bongladesch.onlineshop.request;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sessions")
public class Session implements Serializable
{
	private static final long serialVersionUID = -1382176653004738262L;

	@Id
	@Column(name = "token")
	private String token;

	@Column(name = "username")
	private String userName;

	@Column(name = "admin")
	private boolean admin;

	@Column(name = "timestamp")
	private Calendar timestamp;

	public Session()
	{
		timestamp = new GregorianCalendar();
	}

	/* Getter and Setter */
	
	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getToken()
	{
		return token;
	}

	public void setToken(String token)
	{
		this.token = token;
	}

	public boolean isAdmin()
	{
		return admin;
	}

	public void setAdmin(boolean admin)
	{
		this.admin = admin;
	}

	public Calendar getTimestamp()
	{
		return timestamp;
	}

	public void setTimestamp(Calendar timestamp)
	{
		this.timestamp = timestamp;
	}

	/* Equals, hash and toString */

	@Override
	public int hashCode()
	{
		return this.token.hashCode();
	}
	
	@Override
	public boolean equals(Object other)
	{
		if(other == null) return false;
		if(!(other instanceof Session)) return false;
		Session that = (Session) other;
		return(this.token.equals(that.token));
	}
	
	@Override
	public String toString()
	{
		return "Session of user " + this.userName + " with token " + token;
	}
}