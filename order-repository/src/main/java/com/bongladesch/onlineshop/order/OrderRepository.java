package com.bongladesch.onlineshop.order;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Stateless
@Path("/orders")
public class OrderRepository
{
	private static Logger logger = Logger.getLogger("orderRepositoryLogger");

	@PersistenceUnit(unitName = "order-repository")
	private EntityManagerFactory emf;

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public Response getOrder(@PathParam("id") long id)
	{
		EntityManager em = emf.createEntityManager();
		logger.log(Level.INFO, "Get order with id: " + id);
		Order order = em.find(Order.class, id);
		em.close();
		if(order == null)
		{
			String message = "Order not found with id: " + id;
			logger.log(Level.WARNING, message);
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			String json = mapper.writeValueAsString(order);
			return  Response.ok(json, MediaType.APPLICATION_JSON).build();
		}
		catch(JsonProcessingException e)
		{
			logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for order with id: " + id;
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	@GET
	@Produces("application/json")
	public Response getOrders()
	{
		EntityManager em = emf.createEntityManager();
		logger.log(Level.INFO, "Get list of all orders ordered by date descend");
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Order> cq = cb.createQuery(Order.class);
		Root<Order> rootEntry = cq.from(Order.class);
		CriteriaQuery<Order> all = cq.select(rootEntry).orderBy(cb.desc(rootEntry.get("date")));
		TypedQuery<Order> allQuery = em.createQuery(all);
		List<Order> orders = allQuery.getResultList();
		ObjectMapper mapper = new ObjectMapper();
        try
        {
        	String json = mapper.writeValueAsString(orders);
        	return  Response.ok(json, MediaType.APPLICATION_JSON).build();
        }
        catch(JsonProcessingException e)
        {
        	logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for orders list";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
        }
	}

	@PUT
	@Consumes("application/json")
	@Produces("application/json")
	public Response updateOrder(OrderData data)
	{
		EntityManager em = emf.createEntityManager();
		logger.log(Level.INFO, "Updating order with id: " + data.id);
		Order order = em.find(Order.class, data.id);
		if(order != null)
		{
			Order transientOrder = new Order(data);
			transientOrder.setDate(order.getDate());
			em.merge(transientOrder);
			em.close();
			logger.log(Level.INFO, "Updated order persisted");
		}
		else
		{
			em.close();
			String message = "No order found for id: " + data.id;
			logger.log(Level.SEVERE, message);
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			String json = mapper.writeValueAsString(order);
			return  Response.ok(json, MediaType.APPLICATION_JSON).build();
		}
		catch (JsonProcessingException e)
		{
			logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for order with id: " + data.id;
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response createOrder(OrderData data)
	{
		logger.log(Level.INFO, "Creating order with id \"" + data.id + "\" for user: " + data.userName);
		EntityManager em = emf.createEntityManager();
		Order newOrder = new Order(data);
		Order exists = em.find(Order.class, data.id);
		if(exists == null)
		{
			em.persist(newOrder);
			em.close();
			logger.log(Level.INFO, "Created order persisted");
		}
		else
		{
			em.close();
			String message = "Order already exist with id: " + data.id;
			logger.log(Level.SEVERE, message);
			return Response.status(Response.Status.CONFLICT).entity(getMessageObject(message)).build();
		}
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			String json = mapper.writeValueAsString(newOrder);
			return Response.ok(json, MediaType.APPLICATION_JSON).build();
		}
		catch (JsonProcessingException e)
		{
			logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for order: " + data.id;
			return Response.status(Response.Status.CONFLICT).entity(getMessageObject(message)).build();
		}
	}

	private static String getMessageObject(String message)
	{
		return "{\"message\":\"" + message + "\"}";
	}
}