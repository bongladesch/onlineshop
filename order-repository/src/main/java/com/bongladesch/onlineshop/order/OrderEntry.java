package com.bongladesch.onlineshop.order;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "orderentries")
public class OrderEntry implements Serializable
{
	private static final long serialVersionUID = 5186352794075697453L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "order_id")
	private long orderId;

	@Column(name = "article_id")
	private String articleId;

	@Column(name = "price")
	private double price;

	@Column(name = "number")
	private int number;

	public OrderEntry(OrderEntryData data)
	{
		this.orderId = data.orderId;
		this.articleId = data.articleId;
		this.price = data.price;
		this.number = data.number;
	}

	public OrderEntry()
	{}

	/* Getter and Setter */

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public long getOrderId()
	{
		return orderId;
	}

	public void setOrderId(long orderId)
	{
		this.orderId = orderId;
	}

	public String getArticleId()
	{
		return articleId;
	}

	public void setArticleId(String articleId)
	{
		this.articleId = articleId;
	}

	public double getPrice()
	{
		return price;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	public int getNumber()
	{
		return number;
	}

	public void setNumber(int number)
	{
		this.number = number;
	}

	/* Equals, hash and toString */

	@Override
	public int hashCode()
	{
		return (int)id;
	}

	@Override
	public boolean equals(Object other)
	{
		if(other == null) return false;
		if(!(other instanceof OrderEntry)) return false;
		OrderEntry that = (OrderEntry) other;
		return(getId() == that.getId());
	}

	@Override
	public String toString()
	{
		return "Article: " + this.getArticleId() + ", number: " + getNumber();
	}
}