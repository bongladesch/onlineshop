package com.bongladesch.onlineshop.order;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="orders")
public class Order implements Serializable
{
	private static final long serialVersionUID = -6925111841771290567L;

	@Id
	@Column(name = "id")
	private long id;

	@Column(name = "username")
	private String userName;
	
	@Column(name = "address")
	private long addressId;

	@Column(name = "date")
	private Calendar date;

	@Column(name = "status")
	private String status;

	@OneToMany(cascade = CascadeType.PERSIST)
	@JoinColumn(referencedColumnName = "id", name = "order_id")
	private List<OrderEntry> orderEntries;

	public Order(OrderData data)
	{
		this.id = data.id;
		this.userName = data.userName;
		this.addressId = data.addressId;
		this.date = new GregorianCalendar();
		this.status = data.status;
		if(data.orderEntries != null)
		{
			if(data.orderEntries.size() > 0)
			{
				List<OrderEntry> entries = new ArrayList<>();
				for(int index = 0; index < data.orderEntries.size(); index++)
				{
					OrderEntry entry = new OrderEntry(data.orderEntries.get(index));
					entries.add(entry);
				}
				this.orderEntries = entries;
			}
			else
			{
				this.orderEntries = null;
			}
		}
	}

	public Order()
	{}

	/* Getter and Setter */

	public long getId()
	{
		return id;
	}
	
	public void setId(long id)
	{
		this.id = id;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public long getAddressId()
	{
		return addressId;
	}

	public void setAddressId(long addressId)
	{
		this.addressId = addressId;
	}

	public Calendar getDate()
	{
		return date;
	}

	public void setDate(Calendar date)
	{
		this.date = date;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public List<OrderEntry> getOrderEntries()
	{
		return orderEntries;
	}

	public void setOrderEntries(List<OrderEntry> orderEntries)
	{
		this.orderEntries = orderEntries;
	}

	/* Equals, hash and toString */

	@Override
	public int hashCode()
	{
		return (int)id;
	}

	@Override
	public boolean equals(Object other)
	{
		if(other == null) return false;
		if(!(other instanceof Order)) return false;
		Order that = (Order) other;
		return id == that.id;
	}

	@Override
	public String toString()
	{
		return "Order: " + this.id;
	}
}