#!/bin/bash
set -x

cd admin-service
./gradlew clean
cd ..

cd article-repository
./gradlew clean
cd ..

cd article-service
./gradlew clean
cd ..

cd cart-repository
./gradlew clean
cd ..

cd customer-service
./gradlew clean
cd ..

cd order-repository
./gradlew clean
cd ..

cd order-service
./gradlew clean
cd ..

cd request-handler
./gradlew clean
cd ..

cd search-service
./gradlew clean
cd ..

cd shopping-service
./gradlew clean
cd ..

cd sign-service
./gradlew clean
cd ..

cd user-repository
./gradlew clean
cd ..
