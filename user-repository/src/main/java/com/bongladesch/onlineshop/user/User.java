package com.bongladesch.onlineshop.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "users")
public class User implements Serializable
{
	private static final long serialVersionUID = -6283204433936747058L;

	@Id
	@Column(name = "username")
	private String userName;
	
	@Column(name = "firstname")
	private String firstName;
	
	@Column(name = "lastname")
	private String lastName;
	
	@NotNull
	@Column(name = "email", unique = true)
	private String email;
	
	@NotNull
	@Column(name = "password")
	private String password;
	
	@NotNull
	@Column(name = "admin")
	private boolean admin;
	
	@OneToMany(cascade = CascadeType.PERSIST)
	@JoinColumn(referencedColumnName = "username", name = "username")
	private List<Address> addresses;
	
	public User(UserData data)
	{
		this.userName = data.userName;
		this.firstName = data.firstName;
		this.lastName = data.lastName;
		this.email = data.email;
		this.password = data.password;
		this.admin = data.admin;
		if(data.addresses != null)
		{
			if(data.addresses.size() > 0)
			{
				List<Address> addresses = new ArrayList<>();
				for(int index = 0; index < data.addresses.size(); index++)
				{
					Address address = new Address(data.addresses.get(index));
					addresses.add(address);
				}
				this.addresses = addresses;
			}
			else
			{
				this.addresses = null;
			}
		}
	}
	
	public User()
	{}
	
	/* Getter and Setter */
	
	public boolean isAdmin()
	{
		return this.admin;
	}

	public void setAdmin(boolean isAdmin)
	{
		this.admin = isAdmin;
	}

	public String getFirstName()
	{
		return this.firstName;
	}
	
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	
	public String getLastName()
	{
		return this.lastName;
	}
	
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	
	public String getEmail()
	{
		return this.email;
	}
	
	public void setEmail(String email)
	{
		this.email = email;
	}
	
	public String getUserName()
	{
		return this.userName;
	}
	
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	
	public String getPassword()
	{
		return this.password;
	}
	
	public void setPassword(String password)
	{
		this.password = password;
	}
	
	public List<Address> getAddresses()
	{
		return addresses;
	}
	
	public void setAddresses(List<Address> addresses)
	{
		this.addresses = addresses;
	}
	
	/* Equals, hash and toString */

	@Override
	public int hashCode()
	{
		return this.userName.hashCode();
	}
	
	@Override
	public boolean equals(Object other)
	{
		if(other == null) return false;
		if(!(other instanceof User)) return false;
		User that = (User) other;
		return(this.userName.equals(that.userName));
	}
	
	@Override
	public String toString()
	{
		return "User: " + this.userName;
	}
}
