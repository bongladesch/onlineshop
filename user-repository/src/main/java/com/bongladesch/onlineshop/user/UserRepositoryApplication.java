package com.bongladesch.onlineshop.user;

import javax.ws.rs.core.Application;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("rest")
public class UserRepositoryApplication extends Application
{
	@Override
	public Set<Class<?>> getClasses()
	{
        Set<Class<?>> s = new HashSet<Class<?>>();
        s.add(UserRepository.class);
        return s;
    }
}