package com.bongladesch.onlineshop.user;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * User repository - Micro Service.
 * Repository with database connection to manage
 * all users and their customers addresses.
 * With this service customers, administrators can
 * be created, updated and deleted.
 * @author Ken Brucksch
 *
 */
@Stateless
@Path("/users")
public class UserRepository
{
	private static Logger logger = Logger.getLogger("userRepositoryLogger");
	
	@PersistenceUnit(unitName = "user-repository")
	private EntityManagerFactory emf;

	@GET
	@Path("/{userName}")
	@Produces("application/json")
	public Response getUser(@PathParam("userName") String userName)
	{
		EntityManager em = emf.createEntityManager();
		logger.log(Level.INFO, "Get user with username: " + userName);
		User user = em.find(User.class, userName);
		em.close();
		if(user == null)
		{
			String message = "User not found for username: " + userName;
			logger.log(Level.WARNING, message);
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			String json = mapper.writeValueAsString(user);
			return Response.ok(json, MediaType.APPLICATION_JSON).build();
		}
		catch(JsonProcessingException e)
		{
			logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for user: " + userName;
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}
	
	@GET
	@Path("/customers")
	@Produces("application/json")
	public Response getCustomers()
	{
		EntityManager em = emf.createEntityManager();
		logger.log(Level.INFO, "Get list of all cusomers");
		CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> rootEntry = cq.from(User.class);
        CriteriaQuery<User> all = cq.select(rootEntry).where(cb.equal(rootEntry.get("admin"), false));
        TypedQuery<User> allQuery = em.createQuery(all);
        List<User> customers = allQuery.getResultList();
        ObjectMapper mapper = new ObjectMapper();
        try
        {
        	String json = mapper.writeValueAsString(customers);
        	return  Response.ok(json, MediaType.APPLICATION_JSON).build();
        }
        catch(JsonProcessingException e)
        {
        	logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for customers list";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
        }
	}
	
	@GET
	@Path("/admins")
	@Produces("application/json")
	public Response getAdmins()
	{
		EntityManager em = emf.createEntityManager();
		logger.log(Level.INFO, "Get list of all admins");
		CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> rootEntry = cq.from(User.class);
        CriteriaQuery<User> all = cq.select(rootEntry).where(cb.equal(rootEntry.get("admin"), true));
        TypedQuery<User> allQuery = em.createQuery(all);
        List<User> admins = allQuery.getResultList();
        ObjectMapper mapper = new ObjectMapper();
        try
        {
        	String json = mapper.writeValueAsString(admins);
        	return  Response.ok(json, MediaType.APPLICATION_JSON).build();
        }
        catch(JsonProcessingException e)
        {
        	logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for admins list";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
        }
	}

	@PUT
	@Consumes("application/json")
	@Produces("application/json")
	public Response updateUser(UserData data)
	{
		EntityManager em = emf.createEntityManager();
		logger.log(Level.INFO, "Updating user with username: " + data.userName);
		User user = em.find(User.class, data.userName);
		if(user != null)
		{
			User transientUser = new User(data);
			em.merge(transientUser);
			em.close();
			logger.log(Level.INFO, "Updated user persisted");
		}
		else
		{
			em.close();
			String message = "No user found for username: " + data.userName;
			logger.log(Level.SEVERE, message);
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			String json = mapper.writeValueAsString(user);
			return  Response.ok(json, MediaType.APPLICATION_JSON).build();
		}
		catch (JsonProcessingException e)
		{
			logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for user: " + data.userName;
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}
	
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response registerUser(UserData data)
	{
		logger.log(Level.INFO, "Creating user with username: " + data.userName);
		EntityManager em = emf.createEntityManager();
		User newUser = new User(data);
		User exists = em.find(User.class, data.userName);
		if(exists == null)
		{
			em.persist(newUser);
			em.close();
			logger.log(Level.INFO, "Created user persisted");
		}
		else
		{
			em.close();
			String message = "User already exist with username: " + data.userName;
			logger.log(Level.SEVERE, message);
			return Response.status(Response.Status.CONFLICT).entity(getMessageObject(message)).build();
		}
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			String json = mapper.writeValueAsString(newUser);
			return Response.ok(json, MediaType.APPLICATION_JSON).build();
		}
		catch (JsonProcessingException e)
		{
			logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for user: " + data.userName;
			return Response.status(Response.Status.CONFLICT).entity(getMessageObject(message)).build();
		}
	}
	
	@DELETE
	@Consumes("application/json")
	@Produces("application/json")
	public Response deleteUser(@QueryParam("username") String userName)
	{
		EntityManager em = emf.createEntityManager();
		User user = em.find(User.class, userName);
		if(user != null)
		{
			em.remove(user);
			em.close();
			logger.log(Level.INFO, "Delete user with username: " + userName);
		}
		else
		{
			em.close();
			String message = "No user found for username: " + userName;
			logger.log(Level.SEVERE, message);
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			String json = mapper.writeValueAsString(user);
			return  Response.ok(json, MediaType.APPLICATION_JSON).build();
		}
		catch(JsonProcessingException e)
		{
			logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for user: " + userName;
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}
	
	private static String getMessageObject(String message)
	{
		return "{\"message\":\"" + message + "\"}";
	}
}