package com.bongladesch.onlineshop.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "addresses")
public class Address implements Serializable
{
	private static final long serialVersionUID = -1008353726568820906L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "firstname")
	private String firstName;
	
	@Column(name = "lastname")
	private String lastName;
	
	@Column(name = "username")
	private String userName;
	
	@Column(name = "street")
	private String street;
	
	@Column(name = "number")
	private String number;
	
	@Column(name = "zipcode")
	private String zipcode;
	
	@Column(name = "country")
	private String country;
	
	@Column(name = "city")
	private String city;
	
	public Address(AddressData data)
	{
		this.id = data.id;
		this.firstName = data.firstName;
		this.lastName = data.lastName;
		this.userName = data.userName;
		this.street = data.street;
		this.number = data.number;
		this.zipcode = data.zipcode;
		this.country = data.country;
		this.city = data.city;
	}
	
	public Address()
	{}
	
	/* Getter and Setter */
	
	public long getId()
	{
		return id;
	}
	
	public void setId(long id)
	{
		this.id = id;
	}
	
	public String getFirstName()
	{
		return firstName;
	}
	
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	
	public String getLastName()
	{
		return lastName;
	}
	
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	
	public String getStreet()
	{
		return street;
	}
	
	public void setStreet(String street)
	{
		this.street = street;
	}
	
	public String getUserName()
	{
		return userName;
	}
	
	public void setUserName(String username)
	{
		this.userName = username;
	}
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number)
	{
		this.number = number;
	}
	
	public String getZipcode()
	{
		return zipcode;
	}
	
	public void setZipcode(String zipCode)
	{
		this.zipcode = zipCode;
	}
	
	public String getCountry()
	{
		return country;
	}
	
	public void setCity(String city)
	{
		this.city = city;
	}
	
	public String getCity()
	{
		return city;
	}
	
	public void setCountry(String city)
	{
		this.city = city;
	}
	
	/* Equals, hash and toString */

	@Override
	public int hashCode()
	{
		return (int)this.id;
	}
	
	@Override
	public boolean equals(Object other)
	{
		if(other == null) return false;
		if(!(other instanceof Address)) return false;
		Address that = (Address) other;
		return(getId() == that.getId());
	}
	
	@Override
	public String toString()
	{
		return "Address: " + this.getId() + "from user: " + getUserName();
	}
}