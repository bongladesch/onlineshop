package com.bongladesch.onlineshop.admin;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bongladesch.onlineshop.admin.UserData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Stateless
@Path("/admin")
public class AdminService
{
	private static Logger logger = Logger.getLogger("adminServiceLogger");
	private static final String userTarget = "http://" + getUserRepositoryURL() + ":8080/user-repository";

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response addAdmin(UserData data)
	{
		logger.log(Level.INFO, "Create new admin with username " + data.userName);
		Client client = ClientBuilder.newClient();
		WebTarget webUserTarget = client.target(userTarget).path("/rest/users");
		Invocation.Builder userInvocationBuilder = webUserTarget.request(MediaType.APPLICATION_JSON);
		data.admin = true;
		return userInvocationBuilder.post(Entity.entity(data, MediaType.APPLICATION_JSON));
	}

	@PUT
	@Consumes("application/json")
	@Produces("application/json")
	public Response updateAdmin(UserData data)
	{
		logger.log(Level.INFO, "Update user with username " + data.userName);
		Client client = ClientBuilder.newClient();
		WebTarget webUserTarget = client.target(userTarget).path("/rest/users");
		Invocation.Builder userInvocationBuilder = webUserTarget.request(MediaType.APPLICATION_JSON);
		data.admin = true;
		Response response = userInvocationBuilder.put(Entity.entity(data, MediaType.APPLICATION_JSON));
		if(response.getStatus() == Response.Status.OK.getStatusCode())
		{
			String message = "User with username " + data.userName + " updated";
			return Response.status(Response.Status.OK).entity(getMessageObject(message)).build();
		}
		else
		{
			String message = "Updating admin with username " + data.userName + "failed";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	@GET
	@Produces("application/json")
	public Response viewAdmin(@QueryParam("username")String userName)
	{
		UserData user = getUser(userName);
		user.password = null;
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			String json = mapper.writeValueAsString(user);
			return  Response.ok(json, MediaType.APPLICATION_JSON).build();
		}
		catch(JsonProcessingException e)
		{
			logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for admin: " + userName;
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	@GET
	@Path("/customers")
	@Produces("application/json")
	public Response viewCustomers()
	{
		logger.log(Level.INFO, "Get list of all customers");
		Client client = ClientBuilder.newClient();
		WebTarget webUserTarget = client.target(userTarget).path("/rest/users/customers");
		return webUserTarget.request(MediaType.APPLICATION_JSON).get();
	}

	@GET
	@Path("/admins")
	@Produces("application/json")
	public Response viewAdmins()
	{
		logger.log(Level.INFO, "Get list of all customers");
		Client client = ClientBuilder.newClient();
		WebTarget webUserTarget = client.target(userTarget).path("/rest/users/admins");
		return webUserTarget.request(MediaType.APPLICATION_JSON).get();
	}

	@DELETE
	@Produces("application/json")
	public Response deleteAdmin(@QueryParam("username")String userName)
	{
		return deleteUser(userName);
	}

	@DELETE
	@Path("/customer")
	@Produces("application/json")
	public Response deleteCustomer(@QueryParam("username")String userName)
	{
		Client client = ClientBuilder.newClient();
		String cartTarget = "http://" + getCartRepositoryURL() + ":8080/cart-repository";
		WebTarget webCartTarget = client.target(cartTarget).path("/rest/carts").queryParam("username", userName);
		Invocation.Builder cartInvocationBuilder = webCartTarget.request(MediaType.APPLICATION_JSON);
		Response response = cartInvocationBuilder.delete();
		if(response.getStatus() == Response.Status.OK.getStatusCode())
		{
			return deleteUser(userName);
		}
		else
		{
			String message = "Deletion for admin: " + userName + " failed";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	@PUT
	@Path("/password")
	@Produces("application/json")
	public Response changeCustomerPassword(@QueryParam("username")String userName, @QueryParam("password")String password)
	{
		UserData user = getUser(userName);
		user.password = password;
		return updateAdmin(user);
	}

	private static UserData getUser(String userName)
	{
		logger.log(Level.INFO, "Get admin with username: " + userName);
		Client client = ClientBuilder.newClient();
		WebTarget webUserTarget = client.target(userTarget).path("/rest/users/" + userName);
		Invocation.Builder userInvocationBuilder = webUserTarget.request(MediaType.APPLICATION_JSON);
		Response response = userInvocationBuilder.get();
		UserData data = new UserData();
		return response.readEntity(data.getClass());// TODO Not Found ?
	}

	private static Response deleteUser(String userName)
	{
		Client client = ClientBuilder.newClient();
		WebTarget webUserTarget = client.target(userTarget).path("/rest/users").queryParam("username", userName);
		Response response = webUserTarget.request(MediaType.APPLICATION_JSON).delete();
		if(response.getStatus() == Response.Status.OK.getStatusCode())
		{
			String message = "Admin with username " + userName + " deleted";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
		else
		{
			String message = "Deletion of admin with username " + userName + " failed";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	private static String getUserRepositoryURL()
	{
		String url = System.getenv("USER_REPOSITORY_URL");
		if(url != null) return url;
		else return "user-repository";
	}

	private static String getCartRepositoryURL()
	{
		String url = System.getenv("CART_REPOSITORY_URL");
		if(url != null) return url;
		else return "cart-repository";
	}

	private static String getMessageObject(String message)
	{
		return "{\"message\":\"" + message + "\"}";
	}
}