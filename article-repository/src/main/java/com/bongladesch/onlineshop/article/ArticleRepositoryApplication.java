package com.bongladesch.onlineshop.article;

import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("rest")
public class ArticleRepositoryApplication extends Application
{
	@Override
	public Set<Class<?>> getClasses()
	{
		TimeZone.setDefault(TimeZone.getTimeZone("Europe/Berlin"));
        Set<Class<?>> s = new HashSet<Class<?>>();
        s.add(ArticleRepository.class);
        return s;
    }
}
