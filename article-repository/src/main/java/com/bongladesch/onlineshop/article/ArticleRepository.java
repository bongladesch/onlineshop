package com.bongladesch.onlineshop.article;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Article repository - Micro Service.
 * Repository with database connection to manage
 * all articles.
 * With this service articles can
 * be created, updated and deleted.
 * @author Ken Brucksch
 *
 */
@Stateless
@Path("/articles")
public class ArticleRepository
{
	private static Logger logger = Logger.getLogger("articleRepositoryLogger");

	@PersistenceUnit(unitName = "article-repository")
	private EntityManagerFactory emf;

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public Response getArticle(@PathParam("id") String id)
	{
		EntityManager em = emf.createEntityManager();
		logger.log(Level.INFO, "Get article with id: " + id);
		Article article = em.find(Article.class, id);
		em.close();
		if(article == null)
		{
			String message = "Article not found for id: " + id;
			logger.log(Level.WARNING, message);
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			String json = mapper.writeValueAsString(article);
			return  Response.ok(json, MediaType.APPLICATION_JSON).build();
		}
		catch(JsonProcessingException e)
		{
			logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for article: " + id;
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	@GET
	@Path("/by/{by}")
	@Produces("application/json")
	public Response getLatestOrPopularArticles(@PathParam("by") String by, @QueryParam("from") int from, @QueryParam("to") int to)
	{
		EntityManager em = emf.createEntityManager();
		logger.log(Level.INFO, "Get list of " + by + " articles");
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Article> cq = cb.createQuery(Article.class);
		Root<Article> rootEntry = cq.from(Article.class);
		CriteriaQuery<Article> all;
		if("latest".equals(by))
		{
			all = cq.select(rootEntry).orderBy(cb.desc(rootEntry.get("creation")));
		}
		else if("popular".equals(by))
		{
			all = cq.select(rootEntry).orderBy(cb.desc(rootEntry.get("purchases")));
		}
		else
		{
			String message = "Wrong path argument \"" + by + "\" for this method";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
		TypedQuery<Article> allQuery = em.createQuery(all);
		allQuery.setMaxResults(to);
        List<Article> articles = allQuery.getResultList();
        articles = articles.subList(from, articles.size());
        ObjectMapper mapper = new ObjectMapper();
        try
        {
        	String json = mapper.writeValueAsString(articles);
        	return  Response.ok(json, MediaType.APPLICATION_JSON).build();
        }
        catch(JsonProcessingException e)
        {
        	logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for " + by + " articles list";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
        }
	}

	@GET
	@Path("/category")
	@Produces("application/json")
	public Response getArticlesByCategory(@QueryParam("category") String category, @QueryParam("from") int from, @QueryParam("to") int to)
	{
		EntityManager em = emf.createEntityManager();
		logger.log(Level.INFO, "Get list of articles with category " + category);
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Article> cq = cb.createQuery(Article.class);
		Root<Article> rootEntry = cq.from(Article.class);
		CriteriaQuery<Article> all = cq.select(rootEntry).where(rootEntry.get("categories").in(category));
		TypedQuery<Article> allQuery = em.createQuery(all);
		allQuery.setMaxResults(to);
		List<Article> articles = allQuery.getResultList();
		articles = articles.subList(from, articles.size());
		ObjectMapper mapper = new ObjectMapper();
        try
        {
        	String json = mapper.writeValueAsString(articles);
        	return  Response.ok(json, MediaType.APPLICATION_JSON).build();
        }
        catch(JsonProcessingException e)
        {
        	logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for articles list by category " + category;
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
        }
	}

	@GET
	@Path("/categories")
	@Produces("application/json")
	public Response getCategories()
	{
		EntityManager em = emf.createEntityManager();
		logger.log(Level.INFO, "Get list of all categories ");
		Query query = em.createNativeQuery("select categories from article_categories group by categories");
		@SuppressWarnings("unchecked")
		List<String> categories = query.getResultList();
		for(String s : categories)
		{
			logger.info(s);
		}
		ObjectMapper mapper = new ObjectMapper();
        try
        {
        	String json = mapper.writeValueAsString(categories);
        	return  Response.ok(json, MediaType.APPLICATION_JSON).build();
        }
        catch(JsonProcessingException e)
        {
        	logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for list of categories";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
        }
	}

	@GET
	@Path("/pattern")
	@Produces("application/json")
	public Response getArticlesByPattern(@QueryParam("pattern") String pattern, @QueryParam("from") int from, @QueryParam("to") int to)
	{
		EntityManager em = emf.createEntityManager();
		logger.log(Level.INFO, "Get list of artices which matches on pattern: " + pattern);
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Article> cq = cb.createQuery(Article.class);
		Root<Article> rootEntry = cq.from(Article.class);
		Predicate predicateName = cb.like(cb.upper(rootEntry.<String>get("name")), ("%" + pattern + "%").toUpperCase());
		Predicate predicateId = cb.like(cb.upper(rootEntry.<String>get("id")), ("%" + pattern + "%").toUpperCase());
		Predicate predicate = cb.or(predicateName, predicateId);
		CriteriaQuery<Article> all = cq.select(rootEntry).where(predicate);
		TypedQuery<Article> allQuery = em.createQuery(all);
		allQuery.setMaxResults(to);
		List<Article> articles = allQuery.getResultList();
		articles = articles.subList(from, articles.size());
		ObjectMapper mapper = new ObjectMapper();
        try
        {
        	String json = mapper.writeValueAsString(articles);
        	return  Response.ok(json, MediaType.APPLICATION_JSON).build();
        }
        catch(JsonProcessingException e)
        {
        	logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for articles list matched with pattern " + pattern;
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
        }
	}

	@PUT
	@Consumes("application/json")
	@Produces("application/json")
	public Response updateArticle(ArticleData data)
	{
		EntityManager em = emf.createEntityManager();
		logger.log(Level.INFO, "Updating article with id: " + data.id);
		Article article = em.find(Article.class, data.id);
		if(article != null)
		{
			Article transientArticle = new Article(data);
			transientArticle.setCreation(article.getCreation());
			transientArticle.setPurchases(data.purchases);
			em.merge(transientArticle);
			em.close();
			logger.log(Level.INFO, "Updated article persisted");
		}
		else
		{
			em.close();
			String message = "No article found for id: " + data.id;
			logger.log(Level.SEVERE, message);
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			String json = mapper.writeValueAsString(article);
			return  Response.ok(json, MediaType.APPLICATION_JSON).build();
		}
		catch (JsonProcessingException e)
		{
			logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for article: " + data.id;
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response createArticle(ArticleData data)
	{
		logger.log(Level.INFO, "Creating article with id: " + data.id);
		EntityManager em = emf.createEntityManager();
		Article newArticle = new Article(data);
		Article exists = em.find(Article.class, data.id);
		if(exists == null)
		{
			em.persist(newArticle);
			em.close();
			logger.log(Level.INFO, "Created article persisted");
		}
		else
		{
			em.close();
			String message = "Article already exist with id: " + data.id;
			logger.log(Level.SEVERE, message);
			return Response.status(Response.Status.CONFLICT).entity(getMessageObject(message)).build();
		}
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			String json = mapper.writeValueAsString(newArticle);
			return Response.ok(json, MediaType.APPLICATION_JSON).build();
		}
		catch (JsonProcessingException e)
		{
			logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for article: " + data.id;
			return Response.status(Response.Status.CONFLICT).entity(getMessageObject(message)).build();
		}
	}

	@DELETE
	@Produces("application/json")
	public Response deleteArticle(@QueryParam("id") String id)
	{
		EntityManager em = emf.createEntityManager();
		Article article = em.find(Article.class, id);
		if(article != null)
		{
			em.remove(article);
			em.close();
			logger.log(Level.INFO, "Delete article with id: " + id);
		}
		else
		{
			em.close();
			String message = "No article found for id: " + id;
			logger.log(Level.SEVERE, message);
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			String json = mapper.writeValueAsString(article);
			return  Response.ok(json, MediaType.APPLICATION_JSON).build();
		}
		catch(JsonProcessingException e)
		{
			logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for article: " + id;
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	private static String getMessageObject(String message)
	{
		return "{\"message\":\"" + message + "\"}";
	}
}