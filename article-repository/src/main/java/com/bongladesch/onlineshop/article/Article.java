package com.bongladesch.onlineshop.article;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="articles")
public class Article implements Serializable
{
	private static final long serialVersionUID = -1944825777749606L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "price")
	private double price;

	@Column(name = "stock")
	private long stock;

	@Column(name = "purchases")
	private long purchases;

	@Column(name = "creation")
	private Calendar creation;

	@Column(name = "pictures")
	@ElementCollection
	private List<String> pictures;

	@Column(name = "categories")
	@ElementCollection
	private List<String> categories;

	public Article(ArticleData data)
	{
		this.id = data.id;
		this.name = data.name;
		this.description = data.description;
		this.price = data.price;
		this.stock = data.stock;
		this.purchases = 0;
		this.creation = new GregorianCalendar();
		pictures = new ArrayList<String>();
		for(String picture : data.pictures)
		{
			pictures.add(picture);
		}
		categories = new ArrayList<String>();
		for(String category : data.categories)
		{
			categories.add(category);
		}
	}

	public Article()
	{}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public double getPrice()
	{
		return price;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	public long getStock()
	{
		return stock;
	}

	public void setStock(long stock)
	{
		this.stock = stock;
	}

	public long getPurchases()
	{
		return this.purchases;
	}
	
	public void setPurchases(long purchases)
	{
		this.purchases = purchases;
	}

	public Calendar getCreation()
	{
		return this.creation;
	}

	public void setCreation(Calendar creation)
	{
		this.creation = creation;
	}

	public List<String> getPictures()
	{
		return pictures;
	}

	public void setPictures(List<String> pictures)
	{
		this.pictures = pictures;
	}

	public List<String> getCategories()
	{
		return categories;
	}

	public void setCategories(List<String> categories)
	{
		this.categories = categories;
	}

	/* Equals, hash and toString */

	@Override
	public int hashCode()
	{
		return this.id.hashCode();
	}

	@Override
	public boolean equals(Object other)
	{
		if(other == null) return false;
		if(!(other instanceof Article)) return false;
		Article that = (Article) other;
		return(this.id.equals(that.getId()));
	}

	@Override
	public String toString()
	{
		return "Article: " + this.id;
	}
}