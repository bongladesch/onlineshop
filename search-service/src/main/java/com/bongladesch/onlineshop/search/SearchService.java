package com.bongladesch.onlineshop.search;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("/search")
public class SearchService
{
	private static Logger logger = Logger.getLogger("searchServiceLogger");
	private static final String articleTarget = "http://" + getArticleRepositoryURL() + ":8080/article-repository";

	@GET
	@Produces("application/json")
	public Response getArticle(@QueryParam("id") String id)
	{
		logger.log(Level.INFO, "Get article with id " + id);
		Client client = ClientBuilder.newClient();
		WebTarget webArticleTarget = client.target(articleTarget).path("/rest/articles/" + id);
		return webArticleTarget.request(MediaType.APPLICATION_JSON).get();
	}

	@GET
	@Path("/by/{by}")
	@Produces("application/json")
	public Response getLatestOrPouluarArticles(@PathParam("by") String by, @QueryParam("from") int from, @QueryParam("to") int to)
	{
		logger.log(Level.INFO, "Get " + by + " articles");
		Client client = ClientBuilder.newClient();
		WebTarget webArticleTarget = client.target(articleTarget).path("/rest/articles/by/" + by).queryParam("from", from).queryParam("to", to);
		return webArticleTarget.request(MediaType.APPLICATION_JSON).get();
	}

	@GET
	@Path("category")
	@Produces("application/json")
	public Response getArticleByCategory(@QueryParam("category") String category, @QueryParam("from") int from, @QueryParam("to") int to)
	{
		logger.log(Level.INFO, "Get articles by category " + category);
		Client client = ClientBuilder.newClient();
		WebTarget webArticleTarget = client.target(articleTarget).path("/rest/articles/category").queryParam("category", category).queryParam("from", from).queryParam("to", to);
		return webArticleTarget.request(MediaType.APPLICATION_JSON).get();
	}

	@GET
	@Path("/pattern")
	@Produces("application/json")
	public Response getArticleByPattern(@QueryParam("pattern") String pattern, @QueryParam("from") int from, @QueryParam("to") int to)
	{
		logger.log(Level.INFO, "Get articles by pattern " + pattern);
		Client client = ClientBuilder.newClient();
		WebTarget webArticleTarget = client.target(articleTarget).path("/rest/articles/pattern").queryParam("pattern", pattern).queryParam("from", from).queryParam("to", to);
		return webArticleTarget.request(MediaType.APPLICATION_JSON).get();
	}

	@GET
	@Path("/categories")
	@Produces("application/json")
	public Response getCategories()
	{
		logger.log(Level.INFO, "Get list of all categories");
		Client client = ClientBuilder.newClient();
		WebTarget webArticleTarget = client.target(articleTarget).path("/rest/articles/categories");
		return webArticleTarget.request(MediaType.APPLICATION_JSON).get();
	}

	private static String getArticleRepositoryURL()
	{
		String url = System.getenv("ARTICLE_REPOSITORY_URL");
		if(url != null) return url;
		else return "article-repository";
	}
}