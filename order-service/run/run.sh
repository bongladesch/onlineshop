#!/bin/bash
set -e

# Build war file
./gradlew clean build

compose_file=docker/payara.yml
container_name=order-service

# Start container if container does not exist or is stopped
if [ ! "$(docker ps -q -f name=${container_name})" ]
then
    if [ "$(docker ps -aq -f status=exited -f name=${container_name})" ]
    then
        # cleanup
        docker-compose -f ${compose_file} rm -f
    fi
    # run your container
    docker-compose -p onlineshop -f ${compose_file} up -d
    sleep 1m
fi

# Copy war file to autodeploy folder in container
docker cp build/libs/${container_name}.war ${container_name}:/usr/share/payara41/glassfish/domains/payaradomain/autodeploy/${container_name}.war

echo "Application deployed ..."

# write log messages to console
docker exec ${container_name} tail -F /usr/share/payara41/glassfish/domains/payaradomain/logs/server.log
