package com.bongladesch.onlineshop.order;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("/order")
public class OrderService
{
	private static Logger logger = Logger.getLogger("orderServiceLogger");
	private static final String orderTarget = "http://" + getOrderRepositoryURL() + ":8080/order-repository";

	@GET
	@Produces("application/json")
	public Response viewOrder(@QueryParam("id")long id)
	{
		logger.log(Level.INFO, "Get order with id: " + id);
		OrderData order = getOrder(id);
		return Response.status(Response.Status.OK).entity(order).build();
	}

	@GET
	@Path("/all")
	@Produces("application/json")
	public Response viewOrders()
	{
		logger.log(Level.INFO, "Get all orders");
		Client client = ClientBuilder.newClient();
		WebTarget webOrderTarget = client.target(orderTarget).path("/rest/orders");
		return webOrderTarget.request(MediaType.APPLICATION_JSON).get();
	}

	@PUT
	@Produces("application/json")
	public Response changeOrderState(@QueryParam("id")long id, @QueryParam("status")String status)
	{
		logger.log(Level.INFO, "Change status of order with id: " + id);
		OrderData order = getOrder(id);
		order.status = status;
		Client client = ClientBuilder.newClient();
		WebTarget webOrderTarget = client.target(orderTarget).path("/rest/orders");
		Invocation.Builder orderInvocationBuilder = webOrderTarget.request(MediaType.APPLICATION_JSON);
		Response response = orderInvocationBuilder.put(Entity.entity(order, MediaType.APPLICATION_JSON));
		if(response.getStatus() == Response.Status.OK.getStatusCode())
		{
			String message = "Changed the order status of order " + id + " to " + status;
			return Response.status(Response.Status.OK).entity(getMessageObject(message)).build();
		}
		else
		{
			String message = "Changing order for oder " + id + " status failed";
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	private static OrderData getOrder(long id)
	{
		Client client = ClientBuilder.newClient();
		WebTarget webOrderTarget = client.target(orderTarget).path("/rest/orders/" + id);
		Invocation.Builder orderInvocationBuilder = webOrderTarget.request(MediaType.APPLICATION_JSON);
		Response response  = orderInvocationBuilder.get();
		return response.readEntity(OrderData.class);
	}

	private static String getOrderRepositoryURL()
	{
		String url = System.getenv("ORDER_REPOSITORY_URL");
		if(url != null) return url;
		else return "order-repository";
	}

	private static String getMessageObject(String message)
	{
		return "{\"message\":\"" + message + "\"}";
	}
}