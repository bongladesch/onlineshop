package com.bongladesch.onlineshop.order;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OrderEntryData
{
	@XmlElement public long orderId;
	@XmlElement public String articleId;
	@XmlElement public double price;
	@XmlElement public int number;
}