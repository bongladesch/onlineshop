package com.bongladesch.onlineshop.order;

import java.util.Calendar;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OrderData
{
	@XmlElement public long id;
	@XmlElement public String userName;
	@XmlElement public long addressId;
	@XmlElement public String status;
	@XmlElement public Calendar date;
	@XmlElement public List<OrderEntryData> orderEntries;
}