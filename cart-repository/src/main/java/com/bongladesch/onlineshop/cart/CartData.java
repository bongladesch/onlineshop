package com.bongladesch.onlineshop.cart;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CartData
{
	@XmlElement public String userName;
	@XmlElement public List<CartEntryData> cartEntries;

	public String toString()
	{
		String r = "User: " + userName + "{ ";
		if(cartEntries != null)
		{
			for(CartEntryData entry : cartEntries)
			{
				r += entry.toString() + ", ";
			}
		}
		return r + " }";
	}
}