package com.bongladesch.onlineshop.cart;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "entries")
public class CartEntry implements Serializable
{
	private static final long serialVersionUID = -4777556197367598437L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "username")
	private String userName;
	
	@Column(name = "articleid")
	private String articleId;

	@Column(name = "price")
	private double price;

	@Column(name = "number")
	private int number;
	
	public CartEntry(CartEntryData data)
	{
		this.id = data.id;
		this.userName = data.userName;
		this.articleId = data.articleId;
		this.price = data.price;
		this.number = data.number;
	}
	
	public CartEntry()
	{}
	
	/* Getter and Setter */
	
	public long getId()
	{
		return this.id;
	}
	
	public void setId(long id)
	{
		this.id = id;
	}
	
	public String getUserName()
	{
		return this.userName;
	}
	
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	
	public String getArticleId()
	{
		return this.articleId;
	}
	
	public void setArticleId(String articleId)
	{
		this.articleId = articleId;
	}

	public double getPrice()
	{
		return price;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	public int getNumber()
	{
		return this.number;
	}
	
	public void setNumber(int number)
	{
		this.number = number;
	}
	
	/* Equals, hash and toString */
	
	@Override
	public int hashCode()
	{
		return (int)this.id;
	}
	
	@Override
	public boolean equals(Object other)
	{
		if(other == null) return false;
		if(!(other instanceof CartEntry)) return false;
		CartEntry that = (CartEntry) other;
		return(getId() == that.getId());
	}
	
	@Override
	public String toString()
	{
		return "Cart-Entry: " + this.getId() + "from user: " + getUserName();
	}
}