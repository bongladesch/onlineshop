package com.bongladesch.onlineshop.cart;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CartEntryData
{
	@XmlElement public long id;
	@XmlElement public String userName;
	@XmlElement public String articleId;
	@XmlElement public double price;
	@XmlElement public int number;
	
	public String toString()
	{
		return "ID: " + id + ", username: " + userName + ", articleId: " + articleId + ", number: " + number;
	}
}