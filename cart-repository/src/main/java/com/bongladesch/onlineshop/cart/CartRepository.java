package com.bongladesch.onlineshop.cart;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Stateless
@Path("/carts")
public class CartRepository
{
	private static Logger logger = Logger.getLogger("cartRepositoryLogger");

	@PersistenceUnit(unitName = "cart-repository")
	private EntityManagerFactory emf;

	@GET
	@Path("/{userName}")
	@Produces("application/json")
	public Response getCart(@PathParam("userName") String userName)
	{
		EntityManager em = emf.createEntityManager();
		logger.log(Level.INFO, "Get cart for user with username: " + userName);
		Cart cart = em.find(Cart.class, userName);
		em.close();
		if(cart == null)
		{
			String message = "Cart not found for username: " + userName;
			logger.log(Level.WARNING, message);
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			String json = mapper.writeValueAsString(cart);
			return  Response.ok(json, MediaType.APPLICATION_JSON).build();
		}
		catch(JsonProcessingException e)
		{
			logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for cart of user: " + userName;
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	@PUT
	@Consumes("application/json")
	@Produces("application/json")
	public Response updateCart(CartData data)
	{
		EntityManager em = emf.createEntityManager();
		logger.log(Level.INFO, "Updating cart for user with username: " + data.userName);
		Cart cart = em.find(Cart.class, data.userName);
		if(cart != null)
		{
			Cart transientCart = new Cart(data);
			em.merge(transientCart);
			em.close();
			logger.log(Level.INFO, "Updated cart persisted");
		}
		else
		{
			em.close();
			String message = "No cart for user found with username: " + data.userName;
			logger.log(Level.SEVERE, message);
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			String json = mapper.writeValueAsString(cart);
			return  Response.ok(json, MediaType.APPLICATION_JSON).build();
		}
		catch (JsonProcessingException e)
		{
			logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for cart of user with username: " + data.userName;
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response createCart(@QueryParam("username")String userName)
	{
		logger.log(Level.INFO, "Creating cart for user with username: " + userName);
		EntityManager em = emf.createEntityManager();
		Cart newCart = new Cart(userName);
		Cart exists = em.find(Cart.class, userName);
		if(exists == null)
		{
			em.persist(newCart);
			em.close();
			logger.log(Level.INFO, "Created cart persisted");
		}
		else
		{
			em.close();
			String message = "Cart already exist with username: " + userName;
			logger.log(Level.SEVERE, message);
			return Response.status(Response.Status.CONFLICT).entity(getMessageObject(message)).build();
		}
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			String json = mapper.writeValueAsString(newCart);
			return Response.ok(json, MediaType.APPLICATION_JSON).build();
		}
		catch (JsonProcessingException e)
		{
			logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for cart: " + userName;
			return Response.status(Response.Status.CONFLICT).entity(getMessageObject(message)).build();
		}
	}

	@DELETE
	@Consumes("application/json")
	@Produces("application/json")
	public Response deleteCart(@QueryParam("username") String userName)
	{
		EntityManager em = emf.createEntityManager();
		Cart cart = em.find(Cart.class, userName);
		if(cart != null)
		{
			em.remove(cart);
			em.close();
			logger.log(Level.INFO, "Delete cart for user with username: " + userName);
		}
		else
		{
			em.close();
			String message = "No cart found for username: " + userName;
			logger.log(Level.SEVERE, message);
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			String json = mapper.writeValueAsString(cart);
			return  Response.ok(json, MediaType.APPLICATION_JSON).build();
		}
		catch(JsonProcessingException e)
		{
			logger.log(Level.SEVERE, e.toString());
			String message = "JSON-mapping failed for cart with username: " + userName;
			return Response.status(Response.Status.NOT_FOUND).entity(getMessageObject(message)).build();
		}
	}

	private static String getMessageObject(String message)
	{
		return "{\"message\":\"" + message + "\"}";
	}
}
