package com.bongladesch.onlineshop.cart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "carts")
public class Cart implements Serializable
{
	private static final long serialVersionUID = 5977238816414792670L;

	@Id
	@Column(name = "username")
	private String userName;
	
	@OneToMany(cascade = CascadeType.PERSIST)
	@JoinColumn(referencedColumnName = "username", name = "username")
	private List<CartEntry> cartEntries;
	
	public Cart(CartData data)
	{
		this.userName = data.userName;
		if(data.cartEntries != null)
		{
			if(data.cartEntries.size() > 0)
			{
				List<CartEntry> cartEntries = new ArrayList<>();
				for(int index = 0; index < data.cartEntries.size(); index++)
				{
					CartEntry entry = new CartEntry(data.cartEntries.get(index));
					cartEntries.add(entry);
				}
				this.cartEntries = cartEntries;
			}
			else
			{
				this.cartEntries = null;
			}
		}
	}

	public Cart(String userName)
	{
		this.userName = userName;
		this.cartEntries = new ArrayList<CartEntry>();
	}
	
	public Cart()
	{}
	
	/* Getter and Setter */
	
	public String getUserName()
	{
		return this.userName;
	}
	
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	
	public List<CartEntry> getCartEntries()
	{
		return this.cartEntries;
	}
	
	public void setCartEntries(List<CartEntry> cartEntries)
	{
		this.cartEntries = cartEntries;
	}
	
	/* Equals, hash and toString */
	
	@Override
	public int hashCode()
	{
		return this.userName.hashCode();
	}
	
	@Override
	public boolean equals(Object other)
	{
		if(other == null) return false;
		if(!(other instanceof Cart)) return false;
		Cart that = (Cart) other;
		return(this.userName.equals(that.userName));
	}
	
	@Override
	public String toString()
	{
		return "Cart from user: " + this.userName;
	}
}
